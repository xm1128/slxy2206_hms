package bean;

import lombok.Data;
@Data
public class Admin {
    private Integer id;
    private String adminId;
    private String adminPass;
    private int adminStatement;
}
