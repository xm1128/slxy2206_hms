package bean;
import lombok.Data;
@Data
public class Assess {
    private Integer id;
    private String aFile;
    private String asState;
    private String asReport;
    private String asAnalyze;
    private String asTime;
}