package bean;

import java.util.Date;
import lombok.Data;
@Data
public class Doctor {
    private Integer id;
    private String docId;
    private String docPass;
    private String docName;
    private String docJob;
    private String docSex;
    private Date docAge;
    private String docPhone;
    private String docEmil;
    private String docIdentify;
    private String docPicture;
    private Integer docStatement;
}