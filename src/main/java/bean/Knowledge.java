package bean;
import lombok.Data;
@Data
public class Knowledge {
    private Integer id;
    private String knowTitle;
    private String knowImage;
}