package bean;

import java.util.Date;
import lombok.Data;
@Data
public class Diet {
    private Integer id;
    private String dtType;
    private String dtFood;
    private String dtCalorie;
    private Date dtRecorde;
}