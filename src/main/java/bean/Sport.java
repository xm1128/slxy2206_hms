package bean;
import lombok.Data;
@Data
public class Sport {
    private Integer id;
    private String spoType;
    private String sport;
    private String  spoTime;
    private String spoRecorde;
    private Integer userId;
}