package bean;
import java.util.Date;
import lombok.Data;
@Data
public class User {
    private Integer id;
    private String userId;
    private String userPass;
    private String userName;
    private String height;
    private String weight;
    private Date userAge;
    private String userPhone;
    private String userEmil;
    private String userIdentify;
    private String userSex;
    private Integer userStatement;
}
