package dao;

import bean.User;
        import db.DBManager;

        import java.sql.Connection;
        import java.sql.PreparedStatement;
        import java.sql.ResultSet;
        import java.sql.SQLException;
        import java.util.ArrayList;
        import java.util.List;

public class UserDAO {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        String sql = "select * from user ";
        conn = DBManager.getConnection();
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId(rs.getInt(1));
                u.setUserId(rs.getString(2));
                u.setUserPass(rs.getString(3));
                u.setUserName(rs.getString(4));
                u.setHeight(rs.getString(5));
                u.setWeight(rs.getString(6));
                u.setUserAge(rs.getDate(7));
                u.setUserPhone(rs.getString(8));
                u.setUserEmil(rs.getString(9));
                u.setUserIdentify(rs.getString(10));
                u.setUserSex(rs.getString(11));
                u.setUserStatement(rs.getInt(12));
                list.add(u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
